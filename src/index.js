import uuidv1 from 'uuid/v1';
import 'colors';
import config from '../config';
import getClient from './redis';
import handler from './handler';
import { random, conversion } from './utils/generation';
import wait from './utils/wait';

const { Time: { INERVAL, MAX_TIME } } = config;
const workerId = uuidv1();
const start = async () => {
  const client = await getClient();
  let blockingInfo = conversion(await client.get('blocking'));
  if (!blockingInfo || (blockingInfo && Date.now() - blockingInfo.time > MAX_TIME)) {
    await client.set('blocking', conversion({
      id: workerId,
      time: Date.now(),
    }));
  }
  blockingInfo = conversion(await client.get('blocking'));
  if (blockingInfo && blockingInfo.id === workerId) {
    console.info('Generator'.red);
    await client.set('task', random(11));
  } else {
    console.info('Handler'.green);
    await handler();
  }
};


(async function () {
  setInterval(async () => {
    try {
      const actual = await wait();
      if (actual) {
        await start();
      }
    } catch (e) {
      console.error(e);
    }
  }, INERVAL);
}());
