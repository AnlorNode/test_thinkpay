import { conversion } from './utils/generation';
import getClient from './redis';

export default async () => {
  const client = await getClient();
  const eventsHandled = conversion(await client.get('eventsHandled'));
  if (eventsHandled > 8) {
    await client.set(`Error ${Date.now()}`, 'execution is considered erroneous');
  } else {
    await client.set('eventsHandled', eventsHandled === null ? 0 : eventsHandled + 1);
  }
};
