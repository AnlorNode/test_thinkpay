/* eslint-disable prefer-promise-reject-errors */
/* eslint-disable consistent-return */
import redis from 'redis';
import util from 'util';
import config from '../../config';

let client;
const createClient = reject => {
  if (client && !client.closing) return client;
  client = redis.createClient({
    host: config.Redis.HOST,
    port: config.Redis.PORT,
    retry_strategy: options => {
      if (options.error && options.error.code === 'ECONNREFUSED') {
        return reject('The server refused the connection');
      }
      if (options.total_retry_time > 1000 * 60 * 60) {
        return new Error('Retry time exhausted');
      }
      if (options.attempt > 10) {
        return undefined;
      }
      return Math.min(options.attempt * 100, 3000);
    },
  });
  client.get = util.promisify(client.get);
  client.set = util.promisify(client.set);
  client.keys = util.promisify(client.keys);
  client.mget = util.promisify(client.mget);
};

export default () => new Promise((resolve, reject) => {
  createClient(reject);
  if (client.connected) return resolve(client);
  reject('The server refused the connection');
});
