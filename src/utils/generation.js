export const random = max => Math.floor(Math.random() * Math.floor(max));

export const conversion = data => {
  if (data && typeof data === 'object') {
    return JSON.stringify(data);
  }
  try {
    return JSON.parse(data);
  } catch (e) {
    const num = Number(data);
    if (Number.isNaN(num)) {
      return data;
    }
    return num;
  }
};
