import waitPort from 'wait-port';
import config from '../../config';

export default () => waitPort(
  {
    host: config.Redis.HOST,
    port: config.Redis.PORT,
    interval: 1000,
    output: 'silent',
  },
  3000,
);
