export default {
  Redis: {
    HOST: process.env.REDIS_HOST,
    PORT: Number(process.env.REDIS_PORT),
  },
  Time: {
    INERVAL: 2000,
    MAX_TIME: 7000,
  },
};
