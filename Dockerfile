FROM node:12-alpine

WORKDIR /app

COPY ./package*.json ./
RUN npm i 
COPY ./src ./src
COPY ./.env.example ./.env
COPY ./config.js ./


